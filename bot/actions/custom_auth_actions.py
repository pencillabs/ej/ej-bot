from typing import Any, Dict, List, Text

from ej.auth import AuthenticationDialogue, AuthenticationManager
from ej.conversation import Conversation
from ej.settings import EJCommunicationError
from ej.user import User
from rasa_sdk import Action, FormValidationAction, Tracker
from rasa_sdk.events import EventType
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict


class ValidateAuthenticationForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_authentication_form"

    def validate_has_completed_registration(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        return {}

    def validate_check_authentication(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        if not slot_value:
            return {}

        if AuthenticationDialogue.participant_refuses_to_auth(slot_value):
            return AuthenticationDialogue.end_auth_form(
                has_completed_registration=False
            )

        user = User(tracker)
        user.tracker.slots["access_token"] = ""
        user.tracker.slots["refresh_token"] = ""
        user.authenticate()

        if not user.has_completed_registration:
            dispatcher.utter_message(
                response="utter_error_during_authentication_validation"
            )
            return AuthenticationDialogue.restart_auth_form()

        conversation = Conversation(user.tracker)
        conversation.set_user_statistics()
        return self._get_slots(user, conversation)

    def _get_slots(self, user: User, conversation: Conversation):
        return {
            "user_statistics": conversation.user_statistics,
            "access_token": user.tracker.get_slot("access_token"),
            "refresh_token": user.tracker.get_slot("refresh_token"),
            **AuthenticationDialogue.completed_auth_form(),
        }


class ActionAskCheckAuthentication(Action):
    def name(self) -> Text:
        return "action_ask_check_authentication"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        try:
            user = User(tracker)
            auth_link = AuthenticationManager.get_user_auth_url(user)
            message = AuthenticationDialogue.get_message()
            dispatcher.utter_message(response="utter_get_token", auth_link=auth_link)
            dispatcher.utter_message(**message)

        except EJCommunicationError:
            dispatcher.utter_message(response="utter_ej_communication_error")
            return []
