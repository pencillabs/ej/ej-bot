from enum import Enum
from typing import Any, Dict, Text, List

from actions.logger import custom_logger
from rasa_sdk import FormValidationAction, Tracker, Action
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict


class HelpChoices(Enum):
    """
    Enumerator with a list of valid help options.
    """

    HELP_VOTING = "utter_explain_help_voting"
    HELP_PLAN = "utter_explain_help_plan"
    HELP_KNOW_MORE = "utter_explain_help_know_more"
    HELP_AUTHENTICATION = "utter_explain_help_authentication"
    HELP_LGPD = "utter_explain_help_lgpd"
    END_HELP = "end_help"


class ActionCleanHelpTopicSlot(Action):
    def name(self) -> Text:
        return "action_clean_help_topic_slot"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        return [SlotSet("help_topic", None)]


class ValidateHelpForm(FormValidationAction):
    """
    This action is called when the vote_form is active.
    It shows a comment for user to vote on, and also their statistics in the conversation.

    https://rasa.com/docs/rasa/forms/#using-a-custom-action-to-ask-for-the-next-slot
    """

    def name(self) -> Text:
        return "validate_help_form"

    def validate_help_topic(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        if slot_value is None:
            return {"help_topic": None}
        try:
            CHOICE = HelpChoices[slot_value.upper()]
            if CHOICE != HelpChoices.END_HELP:
                dispatcher.utter_message(response=CHOICE.value)
                return {"help_topic": None}
            return {"help_topic": CHOICE.value}
        except Exception as e:
            custom_logger(f"ERROR: {e}")
            return {"help_topic": HelpChoices.END_HELP.value}
