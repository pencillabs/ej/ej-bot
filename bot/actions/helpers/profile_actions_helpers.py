from dataclasses import dataclass

from actions.base_actions import BaseHelper
from actions.helpers.api_error_checker import EJClientErrorManager
from ej.profile import Profile, ProfileDialogue
from ej.settings import EJCommunicationError
from ej.vote import VoteDialogue
from rasa_sdk.events import SlotSet


@dataclass
class NextProfileQuestionHelper(BaseHelper):
    """
    Request to EJ API the next comment to vote and update the user statistics slots.
    """

    def has_slots_to_return(self) -> bool:
        profile = Profile(self.tracker)

        try:
            message, id = profile.get_next_question()
        except EJCommunicationError:
            ej_client_error_manager = EJClientErrorManager()
            return ej_client_error_manager.get_slots()

        self._dispatch_messages(message)
        self._set_slots(id)
        return True

    def _dispatch_messages(self, message):
        if type(message) is str:
            self.dispatcher.utter_message(message)
        else:
            self.dispatcher.utter_message(**message)

    def _set_slots(self, id):
        self.slots = [
            SlotSet("profile_question_id", id),
            SlotSet("profile_question", None),
        ]


class ValidateProfileQuestionHelper(BaseHelper):
    def has_slots_to_return(self) -> bool:
        profile = Profile(self.tracker)
        profile_question_id = self.tracker.get_slot("profile_question_id")
        profile_question_id = int(profile_question_id)

        try:
            response, err = profile.answer_is_valid(
                self.slot_value, profile_question_id
            )
            if not response:
                if err:
                    ej_client_error_manager = EJClientErrorManager()
                    self.slots = ej_client_error_manager.get_slots(as_dict=True)
                else:
                    self.dispatcher.utter_message(
                        response="utter_invalid_profile_answer"
                    )
                    self._set_slots(is_valid=False)
            else:
                self.dispatcher.utter_message(response="utter_profile_received")
                self._set_slots()
        except ValueError:
            self.slots = ProfileDialogue.restart_profile_form_slots()
            self.dispatcher.utter_message(response="utter_invalid_profile_answer")

        return True

    def _set_slots(self, is_valid=True):
        if is_valid:
            self.slots = {
                **VoteDialogue.restart_vote_form_slots(),
                **Profile.finish_profile(self.slot_value),
            }
        else:
            self.slots = {
                **Profile.continue_profile(),
            }
