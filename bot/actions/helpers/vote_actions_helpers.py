from dataclasses import dataclass

from actions.helpers.api_error_checker import EJClientErrorManager
from actions.helpers.profile_actions_helpers import BaseHelper
from ej.comment import CommentDialogue
from ej.profile import Profile
from ej.settings import EJCommunicationError
from ej.vote import SlotsType, VoteDialogue
from rasa_sdk.events import FollowupAction, SlotSet


@dataclass
class RemainingCommentsSlotsHelper(BaseHelper):
    """
    Check if after a vote, still exists an next comment to vote.
    """

    def has_slots_to_return(self) -> bool:
        self.set_slots()
        return True

    def set_slots(self):
        has_comments_to_vote = self.conversation.has_comments_to_vote()
        if has_comments_to_vote:
            self.dispatcher.utter_message(response="utter_vote_received")
            self.slots = {
                **VoteDialogue.restart_vote_form_slots(),
                "user_statistics": self.conversation.user_statistics,
            }
        else:
            self.dispatcher.utter_message(response="utter_thanks_participation")
            self.slots = VoteDialogue.completed_vote_form_slots(
                self.slots_type, self.conversation.user_statistics
            )


@dataclass
class NextCommentSlotsHelper(BaseHelper):
    """
    Request to EJ API the next comment to vote and update the user statistics slots.
    """

    def has_slots_to_return(self) -> bool:
        try:
            comment = self.conversation.get_next_comment()
            if comment:
                self.set_slots(comment)
            else:
                self.dispatcher.utter_message(response="utter_thanks_participation")
                self.slots = VoteDialogue.completed_vote_form_slots(
                    SlotsType.LIST, self.conversation.user_statistics
                )
        except EJCommunicationError:
            ej_client_error_manager = EJClientErrorManager()
            self.slots = ej_client_error_manager.get_slots()
        return True

    def _dispatch_messages(
        self, comment, user_voted_comments, conversation_total_comments
    ):
        comment_content = comment["content"]
        metadata = self.tracker.latest_message.get("metadata")
        message = CommentDialogue.get_utter_message(
            metadata, comment_content, user_voted_comments, conversation_total_comments
        )
        if type(message) is str:
            self.dispatcher.utter_message(message)
        else:
            self.dispatcher.utter_message(**message)

    def set_slots(self, comment):
        conversation_total_comments = self.conversation.get_total_comments()
        user_voted_comments = self.conversation.get_voted_comments()

        self._dispatch_messages(
            comment, user_voted_comments, conversation_total_comments
        )
        self.slots = [
            SlotSet("user_voted_comments", user_voted_comments),
            SlotSet("comment_content", comment["content"]),
            SlotSet("current_comment_id", comment.get("id")),
        ]


class NeedToAskAboutProfileHelper(BaseHelper):
    """
    Verify if the user needs to answer profile questions.
    """

    def has_slots_to_return(self) -> bool:
        try:
            profile = Profile(self.tracker)
        except EJCommunicationError:
            ej_client_error_manager = EJClientErrorManager()
            self.slots = ej_client_error_manager.get_slots()
            return True
        response, next = profile.need_to_ask_about_profile(
            self.conversation, self.tracker
        )
        if response:
            self.set_slots(next=next)
            return True
        return False

    def set_slots(self, next):
        match self.slots_type:
            case SlotsType.LIST:
                self.slots = [
                    SlotSet("vote", "-"),
                    SlotSet("need_to_ask_profile_question", True),
                    SlotSet("next_count_to_send_profile_question", str(next)),
                    SlotSet("profile_question", None),
                    SlotSet("user_statistics", self.conversation.user_statistics),
                ]
            case SlotsType.DICT:
                self.slots = {
                    "vote": "-",
                    "need_to_ask_profile_question": True,
                    "next_count_to_send_profile_question": str(next),
                    "profile_question": None,
                    "user_statistics": self.conversation.user_statistics,
                }


@dataclass
class ExternalAuthenticationSlotsHelper(BaseHelper):
    """
    Test if the user has reached the anonymous vote limit and needs to authenticate.
    """

    def has_slots_to_return(self) -> bool:
        if self.user.need_to_auth_in_bp(self.conversation):
            self.set_slots()
            return True
        return False

    def set_slots(self):
        match self.slots_type:
            case SlotsType.LIST:
                self.slots = [
                    SlotSet("vote", "-"),
                    SlotSet("ask_to_authenticate", True),
                    SlotSet("user_statistics", self.conversation.user_statistics),
                    FollowupAction("action_deactivate_loop"),
                ]
            case SlotsType.DICT:
                self.slots = {
                    "vote": "-",
                    "ask_to_authenticate": True,
                    "user_statistics": self.conversation.user_statistics,
                }
            case _:
                raise Exception


@dataclass
class UserCanAddCommentsSlotsHelper(BaseHelper):
    """
    Test if the user can add comments to the conversation.
    """

    def has_slots_to_return(self) -> bool:
        if self.conversation.user_can_add_comment(self.tracker):
            self.set_slots()
            return True
        return False

    def set_slots(self):
        self.slots = {
            **CommentDialogue.deactivate_vote_form(self.slot_value),
            "user_statistics": self.conversation.user_statistics,
        }
