from dataclasses import dataclass

from actions.helpers.api_error_checker import EJClientErrorManager
from actions.helpers.profile_actions_helpers import BaseHelper
from ej.auth import AuthenticationManager
from ej.boards import Board
from ej.conversation import Conversation
from ej.settings import EJCommunicationError
from ej.settings import BOARD_ID, CONVERSATION_ID, EJCommunicationError
from ej.user import User
from rasa_sdk.events import SlotSet


@dataclass
class GetConversationSlotsHelper(BaseHelper):
    def has_slots_to_return(self) -> bool:
        if CONVERSATION_ID is not None:
            try:
                conversation_data = Conversation.get(
                    int(CONVERSATION_ID), self.user.tracker
                )
                conversation = Conversation(self.user.tracker, conversation_data)
                conversation.set_user_statistics()
                self.slots = get_setup_slots(conversation, self.user)
            except EJCommunicationError:
                ej_client_error_manager = EJClientErrorManager()
                self.slots = ej_client_error_manager.get_slots()
            return True
        return False


@dataclass
class GetBoardSlotsHelper(BaseHelper):
    def has_slots_to_return(self) -> bool:
        """ """
        ej_client_error_manager = EJClientErrorManager()

        if BOARD_ID is None:
            self.dispatcher.utter_message(response="utter_no_board_id")
            self.slots = ej_client_error_manager.get_slots()
            return True

        try:
            board = Board(int(BOARD_ID), self.user.tracker)
            if len(board.conversations) == 0:
                self.dispatcher.utter_message(response="utter_no_conversations")
                raise Exception("No conversations found.")

            index = 0
            conversation = board.conversations[index]
            self.slots = get_setup_slots(conversation, self.user)
        except EJCommunicationError:
            self.slots = ej_client_error_manager.get_slots()

        return True


def get_setup_slots(conversation: Conversation, user: User):
    return [
        SlotSet(
            "auth_link",
            AuthenticationManager.get_user_auth_url(user),
        ),
        SlotSet("user_statistics", conversation.user_statistics),
        SlotSet(
            "conversation_has_comments_to_vote",
            conversation.has_comments_to_vote(),
        ),
        SlotSet("conversation_id", conversation.id),
        SlotSet("conversation_text", conversation.text),
        SlotSet("anonymous_votes", conversation.anonymous_votes),
        SlotSet(
            "participant_can_add_comments",
            conversation.participant_can_add_comments,
        ),
        SlotSet("send_profile_questions", conversation.send_profile_question),
        SlotSet(
            "votes_to_send_profile_questions",
            conversation.votes_to_send_profile_questions,
        ),
        SlotSet("contact_name", user.name),
        SlotSet(
            "has_completed_registration",
            user.has_completed_registration,
        ),
        SlotSet("access_token", user.tracker.get_slot("access_token")),
        SlotSet("refresh_token", user.tracker.get_slot("refresh_token")),
    ]
