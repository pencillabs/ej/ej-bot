from enum import IntEnum
import json
import os
import random
from typing import Dict, List

from actions.logger import custom_logger
from ej.routes import my_profile_route, profiles_route

from .conversation import Conversation
from .ej_client import EjClient


class ProfileDialogue:
    @classmethod
    def restart_profile_form_slots(cls):
        return {"profile_question": None, "need_to_ask_profile_question": True}


class Profile:
    def __init__(self, tracker):
        self.ej_client: EjClient = EjClient(tracker)
        self.set_profile_fields()
        self.available_questions: Question = self.get_questions()
        self.remaining_questions: List = self.get_remaining_questions()

    def set_profile_fields(self):
        """
        get profile by ej-api
        """
        response = self.ej_client.request(my_profile_route())
        data = response.json()
        self.user = data["user"]
        self.phone_number = data["phone_number"]
        self.ethnicity_choices = data["ethnicity_choices"]
        self.gender = data["gender"]
        self.age_range = data["age_range"]
        self.region = data["region"]

    def get_remaining_questions(self):
        """
        set remaining questions
        """
        remaining_questions = []
        if self.age_range == AgeRange.NOT_FILLED:
            remaining_questions.extend(
                [q for q in self.available_questions if q.change == AgeRange]
            )
        if self.ethnicity_choices == Ethnicity.NOT_FILLED:
            remaining_questions.extend(
                [q for q in self.available_questions if q.change == Ethnicity]
            )
        if self.gender == Gender.NOT_FILLED:
            remaining_questions.extend(
                [q for q in self.available_questions if q.change == Gender]
            )
        if self.region == Region.NOT_FILLED:
            remaining_questions.extend(
                [q for q in self.available_questions if q.change == Region]
            )

        # sort by id
        remaining_questions.sort(key=lambda x: x.id)
        return remaining_questions

    def _get_questions_from_file(self) -> Dict:
        _file = (
            f"{str(os.path.dirname(os.path.realpath(__file__)))}/profile-questions.json"
        )
        data = {}
        with open(_file) as f:
            data = json.load(f)
        return data

    def get_questions(self):
        """
        get questions by profile-questions.json
        """
        data = self._get_questions_from_file()
        self.random_questions = data["random_questions"]
        questions: Question = []

        for question in data["questions"]:
            id = int(question["id"])
            body = question["body"]
            answers = question["answers"]
            tmp_change = question["change"]
            put_payload = question["put_payload"]

            if tmp_change == "Ethnicity":
                change = Ethnicity
            elif tmp_change == "Region":
                change = Region
            elif tmp_change == "Gender":
                change = Gender
            elif tmp_change == "AgeRange":
                change = AgeRange

            questions.append(Question(id, body, answers, change, put_payload))
        return questions

    def get_next_question(self):
        """
        get next question
        """
        if len(self.remaining_questions) == 0:
            raise Exception("No more questions to ask")

        if self.random_questions:
            random_number = random.randint(0, len(self.remaining_questions) - 1)
            return self.remaining_questions.pop(random_number)

        # remaining_questions is sorted by id
        next_question = self.remaining_questions.pop(0)

        message = {"text": next_question.body, "buttons": next_question.answers}
        id = next_question.id

        return message, id

    def need_to_ask_about_profile(self, conversation: Conversation, tracker):
        """
        check if need to ask about profile
        """
        custom_logger("need_to_ask_about_profile")
        if len(self.remaining_questions) == 0:
            custom_logger("NO MORE QUESTIONS")
            return False, -1

        if not conversation.send_profile_question:
            custom_logger("conversation.send_profile_question: False")
            return False, -1

        if conversation.send_profile_question:
            current_votes = conversation.get_voted_comments()
            votes_to_send_profile_questions = (
                conversation.votes_to_send_profile_questions
            )
            next_value_to_send_profile_questions = tracker.get_slot(
                "next_count_to_send_profile_question"
            )

            if not next_value_to_send_profile_questions:
                next_value_to_send_profile_questions = current_votes
            else:
                next_value_to_send_profile_questions = int(
                    next_value_to_send_profile_questions
                )

            if (
                current_votes >= votes_to_send_profile_questions
                and next_value_to_send_profile_questions == current_votes
            ):
                custom_logger("need_to_ask_about_profile: True")
                next_value_to_send_profile_questions += 2
                return True, next_value_to_send_profile_questions
        custom_logger("need_to_ask_about_profile: False")
        return False, -1

    def get_question_from_id(self, id):
        current_question: Question = None
        for available_question in self.available_questions:
            if available_question.id == id:
                current_question = available_question
                break
        return current_question

    def answer_id_is_valid(self, answer, question):
        answer_is_valid = [
            answer_option
            for answer_option in question.answers
            if answer_option["payload"] == answer
        ]
        return len(answer_is_valid)

    def answer_is_valid(self, answer_id, question_id):
        """
        check if answer is valid
        """

        question: Question = self.get_question_from_id(question_id)
        answer_id = int(answer_id)

        err = None
        if self.answer_id_is_valid(answer_id, question):
            response = self.send_answer(answer_id, question)
            if response.status_code == 200:
                return True, err
            else:
                err = response.status_code
                return False, err
        return False, err

    def send_answer(self, answer, question):
        """
        send answer to ej-api
        """
        data = {question.put_payload: answer}
        custom_logger(f"Sending answer {data} to ej-api")
        json_data = json.dumps(data)
        response = self.ej_client.request(self.put_url(), json_data, put=True)
        custom_logger(f"Response: {response.json()}")
        return response

    def put_url(self):
        return f"{profiles_route()}{self.user}/"

    @staticmethod
    def finish_profile(slot_value: str):
        """
        Rasa ends a form when all slots are filled. This method
        fills the profile_form slots with slot_value,
        forcing Rasa to stop sending comments to voting.
        """
        return {"profile_question": slot_value, "need_to_ask_profile_question": False}

    @staticmethod
    def continue_profile():
        """
        Rasa continues the profile form.
        """
        return {"profile_question": None}


class Question:
    def __init__(self, id, body, answers, change, put_payload):
        self.id = id
        self.body = body
        self.answers = answers
        self.change = change
        self.put_payload = put_payload


class Ethnicity(IntEnum):
    NOT_FILLED = 0
    INDIGENOUS = 1
    BLACK = 2
    BROWN = 3
    WHITE = 4
    YELLOW = 5
    PREFER_NOT_TO_SAY = 6


class Region(IntEnum):
    NOT_FILLED = 0
    NORTH = 1
    NORTHEAST = 2
    MIDWEST = 3
    SOUTHEAST = 4
    SOUTH = 5


class Gender(IntEnum):
    NOT_FILLED = 0
    FEMALE = 1
    MALE = 2
    NO_BINARY = 3
    PREFER_NOT_TO_SAY = 20  # need add to ej api


class AgeRange(IntEnum):
    NOT_FILLED = 0
    RANGE_1 = 1
    RANGE_2 = 2
    RANGE_3 = 3
    RANGE_4 = 4
    RANGE_5 = 5
    RANGE_6 = 6
