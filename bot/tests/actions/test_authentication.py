from bot.ej.auth import AuthenticationDialogue, AuthenticationManager
from bot.ej.settings import (
    BP_EJ_COMPONENT_ID,
    EXTERNAL_AUTHENTICATION_HOST,
    JWT_SECRET,
    TOKEN_EXPIRATION_TIME,
)
from datetime import datetime, timezone, timedelta
from bot.ej.user import User
import jwt


class TestCheckAuthenticationDialogue:
    def test_authentication_dialogue_options(self):
        message = AuthenticationDialogue.get_message()
        message[
            "text"
        ] = "Estou aguardando você se autenticar para continuar a votação. 😊"

        buttons = message["buttons"]

        assert buttons[0]["title"] == "Confirmar"
        assert buttons[0]["payload"] == AuthenticationDialogue.CHECK_AUTHENTICATION_SLOT

        assert buttons[1]["title"] == "Encerrar"
        assert buttons[1]["payload"] == AuthenticationDialogue.END_PARTICIPATION_SLOT

    def test_authentication_dialogue_slots(self):
        slots = AuthenticationDialogue.restart_auth_form()
        assert slots["check_authentication"] is None
        assert slots["has_completed_registration"] is None

        slots = AuthenticationDialogue.end_auth_form(has_completed_registration=False)
        assert slots["check_authentication"] == "end_participant_conversation"
        assert slots["has_completed_registration"] == False


class TestAuthenticationManager:
    def test_get_token(self, tracker):
        user = User(tracker)
        expiration_minutes = TOKEN_EXPIRATION_TIME.total_seconds()
        token_expiration = datetime.now(timezone.utc) + timedelta(
            minutes=expiration_minutes
        )
        data = {
            "user_id": user.sender_id,
            "secret_id": user.secret_id,
            "exp": token_expiration,
        }
        token = jwt.encode(data, JWT_SECRET, algorithm="HS256")

        assert token == AuthenticationManager.get_token(user, token_expiration)

    def test_get_url(self, tracker):
        user = User(tracker)
        token = AuthenticationManager.get_token(user)
        assert (
            f"{EXTERNAL_AUTHENTICATION_HOST}/{BP_EJ_COMPONENT_ID}/link_external_user?user_data={token}"
            == AuthenticationManager.get_url(token)
        )

    def test_get_user_auth_url(self, tracker):
        user = User(tracker)
        token = AuthenticationManager.get_token(user)
        url = AuthenticationManager.get_user_auth_url(user)
        assert (
            f"{EXTERNAL_AUTHENTICATION_HOST}/{BP_EJ_COMPONENT_ID}/link_external_user?user_data={token}"
            == url
        )
