from bot.ej.comment import CommentDialogue
from bot.ej.conversation import Conversation
from bot.ej.settings import *
from bot.ej.user import User
from bot.ej.vote import SlotsType, Vote, VoteChoices, VoteDialogue
from rasa_sdk.events import FollowupAction, SlotSet

TOKEN = "mock_token_value"


class TestUtils:
    def test_vote_values_list(self):
        values = ["1", "-1", "0"]
        assert VoteChoices(values[0])
        assert VoteChoices(values[1])
        assert VoteChoices(values[2])

    def test_vote_is_valid(self, tracker):
        assert Vote.is_valid("1")

    def test_vote_is_invalid(self, tracker):
        vote = Vote("xpto", tracker)
        assert not Vote.is_valid("xpto")

    def test_continue_voting(self):
        assert VoteDialogue.restart_vote_form_slots() == {
            "vote": None,
        }

    def test_stop_voting(self, tracker):
        assert VoteDialogue.deactivate_vote_form_slots() == {
            "vote": "-",
        }
        assert VoteDialogue.deactivate_vote_form_slots(SlotsType.LIST) == [
            SlotSet("vote", "-"),
        ]

    def test_finish_voting(self, conversation):
        assert VoteDialogue.completed_vote_form_slots(
            SlotsType.DICT, conversation.user_statistics
        ) == {
            "vote": "-",
            "conversation_has_comments_to_vote": False,
            "user_statistics": conversation.user_statistics,
        }
        assert VoteDialogue.completed_vote_form_slots(
            SlotsType.LIST, conversation.user_statistics
        ) == [
            SlotSet("vote", "-"),
            SlotSet("conversation_has_comments_to_vote", False),
            SlotSet("user_statistics", conversation.user_statistics),
            FollowupAction("action_deactivate_loop"),
        ]

    def test_user_have_comments_to_vote(self, conversation):
        assert conversation.has_comments_to_vote() == True

    def test_define_vote_livechat(self, livechat_metadata):
        message = "vote message"
        utter = CommentDialogue.get_utter_message(livechat_metadata, message, 1, 4)

        assert not "buttons" in utter
        assert "text" in utter

    def test_define_vote_channel_with_buttons(self, metadata):
        message = "vote message"
        utter = CommentDialogue.get_utter_message(metadata, message, 1, 4)

        assert "buttons" in utter
        assert "text" in utter

    def test_remove_special(self):
        assert ":" not in User.normalize_sender_id("sdf:adsf")
        assert "+" not in User.normalize_sender_id("sdf+adsf")
        assert "+" not in User.normalize_sender_id("sdf+:adsf")
        assert ":" not in User.normalize_sender_id("sdf+:adsf")
