import base64
import json

from bot.ej.settings import ANONYMOUS_USER_NAME, SECRET_KEY
from bot.ej.user import UserPasswordManager, User
import pytest


class TestUser:
    def test_get_password_hash(self, tracker):
        user = User(tracker)

        # create a byte string for the password seed
        buffer = f"{user.sender_id}{SECRET_KEY}".encode()

        # encode the seed using base64 lib
        seed_base64 = base64.b64encode(buffer)
        assert len(seed_base64) == 192

        base64_ruby_compatible_format = UserPasswordManager.buffer_to_base64(buffer)
        assert len(base64_ruby_compatible_format) == 196

        # encrypt the base64 seed with sha256 algorithm
        password = UserPasswordManager.to_sha256(base64_ruby_compatible_format)

        assert user._get_password() == password
        assert len(user._get_password()) == 64

    def test_generate_password(self, tracker):
        user = User(tracker)
        assert user.password == user._get_password()
        assert user.password_confirm == user._get_password()

    def test_generate_password_raises_error(self, tracker):
        user = User(tracker)
        user.sender_id = None
        with pytest.raises(Exception):
            user._get_password()

    def test_creating_user(self, tracker):
        user = User(tracker)
        assert user.name == "mr_davidCarlos"
        assert user.email == f"5561981178174-opinion-bot@mail.com"

    def test_serializing_user(self, tracker):
        user = User(tracker)
        user_data = user.registration_data()
        user_dict = json.loads(user_data)
        assert user_dict["name"] == "mr_davidCarlos"
        assert user_dict["name"] == user.name
        assert (
            user_dict["email"]
            == f"{User.normalize_sender_id(tracker.sender_id)}-opinion-bot@mail.com"
        )
        assert user_dict["email"] == user.email
        assert user_dict["has_completed_registration"] == False
        assert (
            user_dict["has_completed_registration"] == user.has_completed_registration
        )

    def test_serializing_user_in_whatsapp_channel(self, tracker):
        user = User(tracker)
        user_data = user.registration_data()
        user_dict = json.loads(user_data)
        assert user_dict["phone_number"] == User.normalize_sender_id(tracker.sender_id)
        assert user_dict["phone_number"] == user.phone_number
        assert len(user.phone_number) == 13

    def test_serializing_user_in_channel_without_number(self, tracker):
        tracker.get_latest_input_channel = lambda: "telegram"
        user = User(tracker)
        user_data = user.registration_data()
        user_dict = json.loads(user_data)
        assert "phone_number" not in user_dict
        assert user.phone_number == ""

    def test_get_username_from_tracker_on_telegram_channel(self, tracker):
        user = User(tracker)
        assert user.name == "mr_davidCarlos"

    def test_get_anonymous_username_from_tracker(self, anonymous_tracker):
        user = User(anonymous_tracker)
        assert user.name == ANONYMOUS_USER_NAME

    def test_get_username_from_tracker_on_whatsapp_channel(self, wpp_tracker):
        user = User(wpp_tracker)
        assert user.name == "David Carlos"
